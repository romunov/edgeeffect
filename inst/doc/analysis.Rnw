\documentclass[a4paper, 12pt, oneside]{article}
\usepackage[tmargin=35mm, bmargin=30mm, lmargin=30mm, rmargin=25mm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[slovene]{babel}
\usepackage[colorlinks = true, urlcolor = red, hidelinks]{hyperref}
\usepackage{amsmath}
\usepackage{float}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{natbib}
\usepackage{etoolbox} % use \thanks{} next to author name to add institution, address, email....

\setlength{\parindent}{0pt} % global \noindent

\title{Bias in estimating density from closed capture Huggins model}
%\title{Blast to the past: Simulation study of correcting for the edge effect for closed captures}
\author{Roman Luštrik and Tomaž Skrbinšek}
\date{\today}

\begin{document}

\maketitle
\tableofcontents

% kako šteti medvede, prezentacija
% http://polarbearscience.com/2013/07/03/guest-post-how-science-counts-bears/
<<setup_chunk>>=
# thm <- knit_theme$get("monochrome") # http://animation.r-forge.r-project.org/knitr/
# knit_theme$set(thm)

opts_chunk$set(size = "small", cache = TRUE, dev = "cairo_pdf",
               fig.show = "hold", fig.width = 10, fig.height = 10,
               out.width = "\\textwidth", tidy = FALSE, comment = NA,
               fig.lp = "fig:")
options(scipen = 6, digits = 4, width = 100)

suppressMessages(library(ggplot2))
suppressMessages(library(reshape2))
suppressMessages(library(dplyr))
suppressMessages(library(gridExtra))
@

It is common to use a model assuming closed captures to estimate population
size. One of the most commonly used models for this is probably Huggins model
for closed captures. A key assumption is equal chance to be captures. If
individuals are not always available for capture, i.e. are not inside the
sampling polygon, this assumption is violated. As a result, the population
estimate does not pertain the sampling area, but rather a larger area.
Object of study is not a population, but rather super population that
transcends primary sampling polygon borders. Most work so far has been done on
surveys where capture has been done using grids of traps. With the advent of new
methods of identification, e.g. identification through DNA available in scats or
urine, captures are no longer confined to predefined points. R offers a simple
environment where data can be generated, assumptions violated and little to no
grant money has to get hurt. We programmed simulations which generates individuals, some of
which violate basic assumptions of closure, performed mark-recapture and
estimated population size.
Functions are available in package \texttt{edgeEffect}. See package vignette how we ran the analysis. This document
shows our train of thought of how we addressed the results and we welcome any
audit and critique to make this work better.\\

The data is available through our package. Run \texttt{data(cc)} to gain access
to the data.frame.\\

Following is a short description of variables. Most of them are returned by
simulation, but some are calculated \textit{post festum} (below the second
horizontal line). For details on calculations of density and difference in true
and estimated densities, see code below.

\clearpage

\begin{tabular}{p{4cm}p{12cm}}
\toprule
variable & description \\
\midrule
\texttt{N} & Estimated population size \\
\texttt{Nlow} and \texttt{Nhigh} & Lower and upper 95\% confidence interval
limits of estimate, respectively. \\
\texttt{pest} & Estimated (re)capture rate \\
\texttt{plow} \& \texttt{phigh} & Lower and upper 95\% confidence interval of
estimate, respectively. \\
\texttt{a} \& \texttt{b} & Parameters determining flatness-roundness of sampling
area.\\
\texttt{r} & Length of the longest dimension of sampling area (two times radius
if $a = b$). \\
\texttt{p} & Parameter used to simulate probability of (re)capture. \\
\texttt{arena} & Radius of arena where centroids of individuals appear. \\
\texttt{N.true} & Simulated number of individuals in arena. \\
\texttt{J} & Number of sampling occassions. \\
\texttt{sigma} & Space use according to a half normal distribution, $\sigma$
corresponds approximately to the radius of home range. \\
model.method & Model used to estimate parameters. \texttt{closedcap} for closed
captures with no heterogeneity. \texttt{heterogen} for closed captures with
heterogeneity. \\
\texttt{mmdm} & Mean maximum distance moved over all individuals captures twice
or more times. \\
\texttt{mmdm.var} & Variance of \texttt{mmdm}. \\
\texttt{seed} & Seed used for random number generation within a simulation. \\
\texttt{ncent.sa} & Number of centroids inside the sampling area. \\
\texttt{ncent.sa.sigma} & Number of centroids inside the sampling area enlarged
for \texttt{sigma}. \\
\texttt{ncent.sa.2sigma} & Number of centroids inside the sampling area enlarged
for 2*\texttt{sigma}. \\
\texttt{ncent.sa.mmdm} & Number of centroids inside the sampling area enlarged
for mean maximum distance moved. \\
\texttt{area.sa} & Size of sampling area. \\
\texttt{area.sa.sigma} & Size of sampling area enlarged for sigma. \\
\texttt{area.sa.mmdm} & Size of sampling area enlarged for mean maximum distance
moved.
\\
\texttt{prop.outside} & Proportion of home ranges (area around centroid enlarged
for sigma, in case of half-normal space use that means 1 standard deviation away
from centroid) completely outside the sampling area. \\
\midrule
\texttt{dens.sa.n} & Estimated number of individuals within sampling area. \\
\texttt{dens.sa.true} & Number of centroids within sampling area. \\
\texttt{dens.sa.sigma.N} & Estimated number of individuals within sampling area
enlarged for sigma. \\
\texttt{dens.sa.sigma.true} & Number of centroids within sampling area enlarged
for simga. \\
\texttt{dens.sa.mmdm.N} & Estimated number of individuals within sampling area
enlarged for mean maximum distance moved. \\
\texttt{dens.sa.mmdm.true} & Number of centroids within sampling area enlarged
for mean maximum distance moved. \\
\bottomrule
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulation with constant sampling area shape}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

These are the settings used to run the simulation. Results are saved in
\texttt{4\_sigma\_simulation\_results\_huggins\_cc.txt}.

A simulation was run on sigma from 1 to 6 where we noticed an interesting (but
expected) behavior. The final simulation has sigma from 1 to 15 (size of
sampling area) with increments of 3.

<<firstim, echo = TRUE, eval = FALSE>>=
setwd("./simulation")
library(devtools)
load_all("q:/workspace/edgeEffect/")

nsim <- 2000
set.seed(357)
runsim <- data.frame(
    a = 1,
    b = 1,
    r = runif(n = nsim, min = 2, max = 15),
    arena = 5,
    world = 40,
    N = round(runif(nsim, min = 20, max = 300)),
    prob = runif(n = nsim, min = 0.1, max = 0.7),
    J = round(runif(n = nsim, min = 4, max = 10)),
    space.use = "halfnormal",
    sigma = sample(c(1, 3, 6, 9, 12, 15), size = nsim, replace = TRUE),
    two.groups = FALSE,
    seed = round(runif(n = nsim, min = 1, max = 1000000)),
    filename = "result_vary_sigma_from_1_to_15_by_3")
result <- vector("list", nsim)

system.time(
    for (i in 1:nsim) {
        if (i %% 100 == 0) {
            message(paste("Iteration number", i, "at", Sys.time()))
            takeout <- list.files(pattern = "mark")
            file.remove(takeout[grepl("mark", takeout)]) # clean once in a while
        }
        result[[i]] <- edgeEffect(
            a = runsim[i, "a"],
            b = runsim[i, "b"],
            r = runsim[i, "r"],
            arena = runsim[i, "arena"],
            world = runsim[i, "world"],
            N = runsim[i, "N"],
            prob = runsim[i, "prob"],
            J = runsim[i, "J"],
            space.use = runsim[i, "space.use"],
            sigma = runsim[i, "sigma"],
            two.groups = runsim[i, "two.groups"],
            seed = runsim[i, "seed"],
            verbose = FALSE,
            filename = runsim[i, "filename"]
        )
    })
@

\clearpage
<<import_data>>=
cc <- read.table("./data/result_vary_sigma_from_1_to_15_by_3 _cc.txt",
                 header = TRUE, sep = " ")
@

<<exploreunivariate, echo = FALSE, fig.width = 8, fig.height = 8>>=
table(cc$warning) # see how many warnings were produced
par(mfrow = c(2, 2))
hist(cc$p, main = "Histogram of estimated p")
hist(cc$N.true, main = "Histogram of simulated N")
hist(cc$N, main = "Histogram of estimated N")

# remove cases where estimation was not successful
cc <- cc[!is.na(cc$N), ]

# remove what would be considered rediculous estimates and confidence intervals
high.N.estimates <- cc$N > max(cc$N.true) * 3
cc <- cc[!high.N.estimates, ]

cc <- cc[cc$Nlow < 800, ]
cc <- cc[cc$Nhigh < 800, ]

hist(cc$N, main = "Removed extreme estimates of N")
@

Above figure shows histograms of simulated $p$ and $N$
(top row). Bottom row shows values of estimated $N$, with left showing all the values
and the right frame is a histogram of values after
\Sexpr{table(high.N.estimates)["TRUE"]} extreme values have been omitted.

\clearpage
<<ngivenp, echo = FALSE, fig.width = 7, fig.height = 4>>=
ngp1 <- ggplot(cc, aes(x = N.true, color = p)) +
    theme_bw() +
    #   scale_y_continuous(limits = c(0, 400)) +
    geom_pointrange(aes(ymin = Nlow, ymax = Nhigh, y = N), shape = 1)

ngp2 <- ggplot(cc, aes(x = N.true, y = N, color = p)) +
    theme_bw() +
    geom_point(size = 2, shape = 1) +
    geom_line(aes(x = N.true, y = N.true), shape = 1, color = "black")

grid.arrange(ngp2, ngp1, ncol = 2)
@

Above left figure shows population size estimate ($y$ axis) with
simulated population size ($x$ axis). Color indicates simulated $p$ value. Lines
represent 95\% confidence interval of populatoin size estimate. The $y$
axis is cropped to limits of $(0, 400)$, but some intervals extend beyond this
(see code). Our estimates seem to be, on average, more or less correct,
although some intervals are quite broad (see figure on the right).
Figure on the right shows same data as previous figure, with the difference that
estimate confidence intervals are not shown. It can be seen that estimates of
population size are correct, with some deviation. With larger simulated
population, it is expected to get larger variance in estimates  population size.

\clearpage
<<truenbynbyj, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = N.true, y = N, color = J)) +
    theme_bw() +
    geom_point(size = 2, shape = 1) +
    geom_line(aes(x = N.true, y = N.true), size = 0.5, color = "black") +
    facet_wrap(~ sigma)
@

Above figure shows dependence between simulated number of individuals ($x$ axis)
an estimated number of individuals ($y$ axis). Data points are colored by number of
sampling occassions ($J$). It would appear there is no obvious pattern between
number of sampling session and precision of estimate. With increasing sigma, the
variance appears to increase, mostly down for small sigma (1) and in both
directions for larger (e.g. sigma 15).

\clearpage
<<pbypestbyj, echo = FALSE, fig.width = 7, fig.height = 4>>=
pest1 <- ggplot(cc, aes(x = p, y = pest, color = J)) +
    theme_bw() +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_point(size = 2, shape = 1)

pest2 <- ggplot(cc, aes(x = p, color = J)) +
    theme_bw() +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_pointrange(aes(ymin = plow, ymax = phigh, y = pest), shape = 1, alpha = 0.5)

grid.arrange(pest1, pest2, ncol = 2)
@

Above left figure shows dependence between simulated ($x$ axis) and estimated ($y$ axis)
probability of (re)capture. While the figure axes are limited to interval
between $(0, 1)$, the data has been simulated on a narrower interval (see
simulation parameters). Probability of capture normally isn't (much) higher than
simulated, however significantly lower estimates are common. We attribute
this phenomenon to individuals who are simulated to have $p$, but the parameter
is modified by $\tau$ (sensu \citep{kendall1999}) because they are not always
available for capture (they cross the sampling area poylgon). Number of sampling
occassions, again, shows no obvious patterns. Figure on the right shows same
data, with additional 95\% confidence intervals of estimates.

\clearpage
<<pgivenr, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = p, y = pest, color = r)) +
    theme_bw() +
    geom_abline(intercept = 0, slope = 1) +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_point(size = 2, shape = 1)
@

Above figure indicates estimates of $p$ comes closest to the
simulated value (black line). Area of sampling ($r$) seems to be related to bias
associated with estimating $p$. Small sampling area results in more bias

\clearpage
<<pgivenrfacet, echo = FALSE, fig.width = 8, fig.height = 5>>=
ggplot(cc, aes(x = p, y = pest, color = r)) +
    theme_bw() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
    geom_abline(intercept = 0, slope = 1) +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_point(size = 2, shape = 1) +
    facet_wrap(~ sigma)
@

Above figure shows how simulated ($x$ axis) and estimated ($y$ axis) $p$ values
correlate, facetted by sigma (home range size, rows).
%and number of centroids inside
%sampling area and expanded sampling area (sigma, 2 * sigma, mmdm, columns).
For all cases of sigma (facets), a clear pattern is evident. Bigger
sampling area is associated with less biased estimates of $p$ and bias is
smallest for sigma of one and is progressively larger as sigma increases.

\clearpage
Substract estimated population size from simulated, including confidence
intervals. It should be worth keeping in mind that the population was simulated
in \texttt{arena} and not sampling area. Looking at population size may thus be
erronous. For this reason we investigate density further down.

<<calculate_delta>>=
cc$N.delta <- cc$N.true - cc$N
cc$N.delta.low <- cc$N.true - cc$Nlow
cc$N.delta.high <- cc$N.true - cc$Nhigh
@

<<ndelta, echo = FALSE, fig.width = 7, fig.height = 4>>=
ndelta.pts <- ggplot(cc, aes(x = N.true, y = N.delta, color = p)) +
    theme_bw() +
    geom_point(size = 2, shape = 1) +
    stat_smooth(color = "red", size = 1, method = "loess")

ndelta.rng <- ggplot(cc, aes(x = N.true, y = N.delta, color = p)) +
    theme_bw() +
    geom_pointrange(aes(ymin = N.delta.low, ymax = N.delta.high), shape = 1) +
    stat_smooth(color = "red", size = 1, method = "loess")

grid.arrange(ndelta.pts, ndelta.rng, ncol = 2)
@

Above figure shows the difference (\texttt{N.delta}) between simulated (\texttt{N.true})
number of animals and its estimate. There is a slight increase in the difference
as the number of simulated animals increases. Capture probability $p$ appears to
exhibit no obivous patterns.

\clearpage
<<rdeltasigma, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = r, y = N.delta)) +
    theme_bw() +
    geom_vline(xintercept = 5, linetype = "dashed", color = "grey") +
    geom_pointrange(aes(ymin = N.delta.low, ymax = N.delta.high), alpha = 0.5, shape = 1) +
    geom_smooth(color = "red", size = 0.6, method = "loess") +
    facet_wrap(~ sigma)
@

Figure above shows dependece of difference between simulated and
estimated number of individuals and size of sampling area.

\clearpage
<<melt_ncent>>=
cc.ncent <- melt(cc,
                 measure.vars = c("ncent.sa", "ncent.sa.sigma", "ncent.sa.mmdm"),
                 variable.name = "ncent")
@

<<num_cent_by_compensation, echo = FALSE, fig.width = 10, fig.height = 6>>=
ggplot(cc.ncent, aes(x = r, y = value - N)) +
    theme_bw() +
    geom_vline(xintercept = 5, color = "grey", linetype = "dashed") +
    geom_point(alpha = 0.5, size = 2, shape = 1) +
    ylab("Number of centroids inside sa/sa.sigma/sa.mmdm - N") +
    geom_smooth(se = FALSE, color = "red", method = "loess") +
    facet_wrap(~ ncent + sigma, ncol = 6)
@

Figure above shows number of centroids reduces by
estimated number of individuals in relation to sampling area size. Data are
sliced according to the way the original sampling area is enlarged (rows; for
sigma and MMDM) and sigma (columns).
Vertical dashed lines marks the radius of \texttt{arena} where centroids were
simulated.
Bias is smallest for sigma 1 (first column) and increases with the size of
sigma (columns). Enlarging the sampling area (rows) for sigma and MMDM lowers
the bias, but is still evident. Biggest effect seems to be when sampling area is enlarged for
MMDM for larger home range sizes (sigma 6, 9, 12 and 15).

\clearpage
Simulated density is calculated by counting the number of centroids inside the
sampling area (\texttt{ncent.*}) and dividing the number by sampling area or sampling area
enlarged for sigma or MMDM. Density is calculated as the estimated number of
individuals (\texttt{N}) divided by area (same as for simulated data). Difference
in densities are calculated as difference between estimated and simulated number
of individuals within a certain area, be that sampling area or sampling area
enlarged for sigma or MMDM.

<<calculatedens>>=
# SA = sampling area
#dens.sa.N = density in SA using N (estimated number of individuals)
cc$dens.sa.N <- with(cc, N/area.sa)
# density in sampling area using true number of centroids inside SA
cc$dens.sa.true <- with(cc, ncent.sa/area.sa)  # true density
# density in SA enlarged for sigma using N
cc$dens.sa.sigma.N <- with(cc, N/area.sa.sigma)
# density in SA enlarged for sigma using true number of centroids inside SA
cc$dens.sa.sigma.true <- with(cc, ncent.sa.sigma/area.sa.sigma) # true density
# density in SA enlarged for MMDM using N
cc$dens.sa.mmdm.N <- with(cc, N/area.sa.mmdm)
# density in SA enlarged for MMDM using true number of centroids inside SA
cc$dens.sa.mmdm.true <- with(cc, ncent.sa.mmdm/area.sa.mmdm) # true density

cc$dens.sa <- with(cc, dens.sa.N - dens.sa.true)
cc$dens.sa.sigma <- with(cc, dens.sa.sigma.N - dens.sa.sigma.true)
cc$dens.sa.mmdm <- with(cc, dens.sa.mmdm.N - dens.sa.mmdm.true)

dens.cc <- melt(cc, measure.vars = c("dens.sa", "dens.sa.sigma", "dens.sa.mmdm"))
@

\clearpage
<<facetbysigma, echo = FALSE, fig.height = 12, fig.width = 8, out.width = "0.8\\linewidth", fig.align = "center">>=
ggplot(dens.cc, aes(x = r, y = value)) +
    theme_bw() +
    ylab("Difference in densities") +
    geom_point(size = 2, shape = 1, alpha = 0.5) +
    geom_smooth(se = FALSE, color = "red", method = "loess") +
    geom_vline(xintercept = unique(cc$arena), size = 0.25) +
    facet_wrap(~ sigma, ncol = 1)
@

Figure above shows difference in density estimates in relation to
size of sampling area (see above for how the difference is calculated). If
no bias would be present, values should lie close to zero. Bias
becomes negligible at around sampling area size of 5 (vertical black line). It
is worth keeping in mind that these are pooled data for all estimates, including
those where sampling area has been enlarged for sigma or MMDM. See next figure.

\clearpage
<<dens, echo = FALSE, fig.width = 11, fig.height = 11>>=
ggplot(dens.cc, aes(x = r, y = value)) +
    theme_bw() +
    ylab("Estimated density - simulated density of centroids") +
    geom_point(size = 2, shape = 1, alpha = 0.5) +
    geom_smooth(se = FALSE, color = "red", method = "loess") +
    geom_vline(xintercept = unique(cc$arena), size = 0.25) +
    facet_wrap(~ sigma + variable, ncol = 3)
@

Figure above shows difference in simulated and estimated density in
relation to sampling area size. Figure is sliced into row for sigma and
columns for enlarged sampling area (raw sampling area, enlarged for sigma,
enlarged for MMDM).
Bias for raw densities (estimated number of animals divided by the sampling
area) is evident for all home range sizes (sigma; first column). By using
enlarged sampling area to calculate density, we improve on the bias, as evident
in the second (enlarged for sigma) and third column (enlarged for MMDM).
On average, there seems to be some bias for home range size (sigma) 1 and 2 for
both enlarged sampling areas for sigma and MMDM, but as home range size
increases, the bias seems to disappear, at least for calculations where sampling
area was enlarged for sigma (second column).\\

Variable \texttt{prop.outside} gives the proportion of individuals with home
range completely outside.

\clearpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulation with varying sampling area shape}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this chapter we focused on how varying shape of sampling polygon influences
the estimates. For comparison, a circular, slightly flatted and highly flattened
(ellipse) sampling polygons were constructed (by controling the \texttt{a} and
\texttt{b} parameters of the \texttt{edgeEffect} function). Variable to hold
this information is \texttt{sigma} and the lower the number, the higher the
symmetry.

This are the settings used to run the simulation.
<<ellipsesim, echo = TRUE, eval = FALSE>>=
setwd("./simulation")
library(devtools)
load_all("q:/workspace/edgeEffect/")

nsim <- 2000
set.seed(762)
runsim <- data.frame(
    a = 1,
    b = sample(c(1, 2, 5, 15), size = nsim, replace = TRUE),
    r = runif(n = nsim, min = 2, max = 15),
    arena = 5,
    world = 40,
    N = round(runif(nsim, min = 20, max = 300)),
    prob = runif(n = nsim, min = 0.1, max = 0.7),
    J = round(runif(n = nsim, min = 4, max = 10)),
    space.use = "halfnormal",
    sigma = sample(1:6, size = nsim, replace = TRUE),
    two.groups = FALSE,
    seed = round(runif(n = nsim, min = 1, max = 1000000)),
    filename = "result_vary_sa_shape_from_circle_to_ellipse")
result <- vector("list", nsim)

system.time(
    for (i in 1:nsim) {
        if (i %% 100 == 0) {
            message(paste("Iteration number", i, "at", Sys.time()))
            takeout <- list.files(pattern = "mark")
            file.remove(takeout[grepl("mark", takeout)]) # clean once in a while
        }
        result[[i]] <- edgeEffect(
            a = runsim[i, "a"],
            b = runsim[i, "b"],
            r = runsim[i, "r"],
            arena = runsim[i, "arena"],
            world = runsim[i, "world"],
            N = runsim[i, "N"],
            prob = runsim[i, "prob"],
            J = runsim[i, "J"],
            space.use = runsim[i, "space.use"],
            sigma = runsim[i, "sigma"],
            two.groups = runsim[i, "two.groups"],
            seed = runsim[i, "seed"],
            verbose = FALSE,
            filename = runsim[i, "filename"]
        )
    })
@

Here is a visualization of how sampling area changes by modifying parameters
\texttt{a} and \texttt{b}.

<<samarea_visualization, echo = FALSE>>=
library(car)
a <- 1
b <- 1
r <- 10
lim <- 50
plot(-lim:lim, -lim:lim, type = "n", asp = 1)
ellipse(center = c(0,0), shape = matrix(c(a, 0,0, b), ncol = 2),
        center.pch = 0, radius = r, draw = TRUE, segments = 500)

b <- 2
ellipse(center = c(0,0), shape = matrix(c(a, 0,0, b), ncol = 2),
        center.pch = 0, radius = r, draw = TRUE, segments = 500)

b <- 5
ellipse(center = c(0,0), shape = matrix(c(a, 0,0, b), ncol = 2),
        center.pch = 0, radius = r, draw = TRUE, segments = 500)

b <- 15
ellipse(center = c(0,0), shape = matrix(c(a, 0,0, b), ncol = 2),
        center.pch = 0, radius = r, draw = TRUE, segments = 500)
@

<<import_data_vary_sa>>=
cc <- read.table("./data/result_vary_sa_shape_from_circle_to_ellipse _cc.txt",
                 header = TRUE, sep = " ")

# change sigma to human readable form
cc$sigma <- factor(cc$sigma, ordered = TRUE)
cc$sigma <- plyr::mapvalues(cc$sigma, from = c("1", "3", "6", "9", "12","15"),
                            to = c("circle", "elli3", "elli6", "alli9", "elli12", "elli15"))
@

<<explore_univariate_vary_sa, echo = FALSE, fig.width = 8, fig.height = 8>>=
table(cc$warning) # see how many warnings were produced
par(mfrow = c(2, 2))
hist(cc$p, main = "Histogram of estimated p")
hist(cc$N.true, main = "Histogram of simulated N")
hist(cc$N, main = "Histogram of estimated N")

# remove what would be considered rediculous estimates
high.N.estimates <- cc$N > max(cc$N.true) * 2
cc <- cc[!high.N.estimates, ]

cc <- cc[cc$Nlow < 800, ]
cc <- cc[cc$Nhigh < 800, ]

hist(cc$N, main = "Removed extreme estimates of N")
par(mfrow = c(1, 1))
@

Above figure shows histograms of simulated $p$ and $N$
(top row). Bottom row shows values of estimated $N$, with left showing all the values
and the right frame is a histogram of values after
\Sexpr{table(high.N.estimates)["TRUE"]} extreme values have been omitted.

\clearpage
<<ngivenp_vary_sa_2, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = N.true, y = N, color = p)) +
    theme_bw() +
    geom_point(size = 2, shape = 1) +
    geom_line(aes(x = N.true, y = N.true), size = 0.5, color = "black") +
    facet_wrap(~ sigma)
@

Above figure shows simulated population size ($x$ axis) and its estimate ($y$ axis).
Notice that estimates of population size are correct on average.
There appears to be no obvious pattern that $p$ values influence the
precission of estimates. However, for smaller sigma (e.g. 1), number of
individuals seems to be underestimated. For larger sigma (e.g. 15) the bias
seems to go both ways, over- and underestimating the number of individuals.

\clearpage
<<ngivenp_vary_sa, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = N.true, color = p)) +
    theme_bw() +
    geom_pointrange(aes(ymin = Nlow, ymax = Nhigh, y = N), shape = 1, alpha = 0.5) +
    facet_wrap(~ sigma)
@

Above figure shows same data with addition of 95\% confidence intervals. Notice
the intervals get larger with decreasing circularity (cf. sigma 1 and 15).

\clearpage
<<truenbynbyj_vary_sa, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = N.true, y = N, color = J)) +
    theme_bw() +
    geom_point(size = 2, shape = 1) +
    geom_line(aes(x = N.true, y = N.true), shape = 1, color = "black") +
    facet_wrap(~ sigma)
@

Above figure shows dependency between simulated number
of individuals and an estimate. Data points are gradient colored by the
number of sampling occassions ($J$). There seems to be no pattern that lower
number of occassions would yield worse estimates.

\clearpage
<<pbypestbyjint_vary_sa, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = p, y = pest, color = J)) +
    theme_bw() +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_abline(intercept = 0, slope = 1) +
    geom_point(size = 2, shape = 1) +
    facet_wrap(~ sigma)
@

Above figure shows dependence between simulated and
estimated probability of (re)capture (model assumes probability of capture (p)
and recapture (c) are identical). The data are sliced according to home
range shape (sigma). While the figure axes are limited to interval between $(0,
1)$, the data has been simulated on a narrower interval.
Probability of capture is often ``on the mark'' but can get lower, especially
as circle becomes a flattened ellipse.
We attribute this phenomenon to individuals who are simulated to have $p$, but
the probability is modified by $\psi$ because they are not always available for
capture (they cross the sampling area poylgon). The phenomenon is exacerbated by
increased sigma. There appears to be no obvious pattern due to number of
sampling occassions.

\clearpage
<<pbypestbyjint_vary_sa_int, echo = FALSE, fig.width = 7, fig.height = 4>>=
ggplot(cc, aes(x = p, color = J)) +
    theme_bw() +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_abline(intercept = 0, slope = 1) +
    geom_pointrange(aes(ymin = plow, ymax = phigh, y = pest), size = 0.3, alpha = 0.2) +
    facet_wrap(~ sigma)
@

Above figure shows same data, with additional 95\% confidence intervals of
estimates. Size of intervals appears to be constant for all sigmas (unlike
differences between simulated and estimated population size).

\clearpage
<<pgivenr_vary_sa, echo = FALSE>>=
ggplot(cc, aes(x = p, y = pest, color = r)) +
    theme_bw() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
    geom_abline(intercept = 0, slope = 1) +
    scale_x_continuous(limits = c(0, 1)) +
    scale_y_continuous(limits = c(0, 1)) +
    geom_point(size = 1.25) +
    facet_wrap(~ sigma)
@

Above figure shows how estimates of $p$ come closest to
true simulated values (black line). Bias seems to increase (downward) as
sampling area (sigma) goes from circular to more ellipse. Clear divergence from
expected estimate is seen when home range increases (cf. sigma=1 vs sigma=15).
For smaller sampling areas, the estimated probability of capture is especially
biased (down).

\clearpage
Substract estimated population size from simulated, including confidence
intervals. It should be worth keeping in mind that the population was simulated
in \texttt{arena} and not sampling area. Looking at population size may thus be
erronous. For this reason we investigate density further down.

<<ndelta_vary_sa_calc>>=
cc$N.delta <- cc$N.true - cc$N
cc$N.delta.low <- cc$N.true - cc$Nlow
cc$N.delta.high <- cc$N.true - cc$Nhigh
@

<<ndelta_vary_sa, echo = FALSE>>=
ggplot(cc, aes(x = N.true, y = N.delta, color = p)) +
    theme_bw() +
    geom_point(shape = 1, size = 2) +
    stat_smooth(color = "red", size = 1, method = "loess", se = FALSE) +
    facet_wrap(~ sigma)
@

Figure shows the difference in simulated and estimated population size (termed
bias) for different sigmas (home range size). Gradient color indicates
probability of capture. There appears to be no discernable pattern regarding
bias due to sigma or $p$.

<<ndelta_vary_sa_2, echo = FALSE>>=
ggplot(cc, aes(x = N.true, y = N.delta, color = p)) +
    theme_bw() +
    geom_pointrange(aes(ymin = N.delta.low, ymax = N.delta.high), shape = 1) +
    stat_smooth(color = "red", size = 1, method = "loess", se = FALSE) +
    facet_wrap(~ sigma)
@

Same figure as above, with additional 95\% confience intervals shown. As for
figure on page \pageref{ngivenp_vary_sa}, confidence intervals seem to get
larger as home range size (sigma) increases.

\clearpage
<<rdeltasigma_vary_sa, echo = FALSE>>=
ggplot(cc, aes(x = r, y = N.delta)) +
    theme_bw() +
    geom_vline(xintercept = 5, linetype = "dashed", color = "grey") +
    geom_pointrange(aes(ymin = N.delta.low, ymax = N.delta.high), alpha = 0.5, shape = 1) +
    geom_smooth(color = "red", size = 0.6, method = "loess") +
    facet_wrap(~ sigma)
@

Above figure shows a pattern where the difference of
estimated and simulated population size change depending on \texttt{sigma}.
There seem to be higher discrepancies for smaller \texttt{sigma}.

\clearpage
<<melt_ncent_vary_sa>>=
cc.ncent <- melt(cc,
                 measure.vars = c("ncent.sa", "ncent.sa.sigma", "ncent.sa.mmdm"),
                 variable.name = "ncent")
@

<<num_cent_by_compensation_vary_sa, echo = FALSE>>=
ggplot(cc.ncent, aes(x = r, y = value - N)) +
    theme_bw() +
    geom_vline(xintercept = 5, color = "grey", linetype = "dashed") +
    geom_point(alpha = 0.5, size = 2, shape = 1) +
    ylab("Number of centroids inside sa/sa.sigma/sa.mmdm - N") +
    geom_smooth(se = FALSE, color = "red", method = "loess") +
    facet_wrap(~ ncent + sigma, ncol = 6)
@

Above figure shows number of centroids reduces by estimated number of
individuals in relation to sampling area size. Figure is latticed according to
the way the original sampling area is enlarged for sigma and MMDM.

\clearpage
Density is calculated as difference between estimated and simulated
number of individuals within a certain area, be that sampling area or sampling
area enlarged for sigma or MMDM. Simulated density is calculated by counting the
number of centroids inside the sampling area and dividing the number by sampling
area or sampling area enlarged for sigma or MMDM.

<<calculatedens_vary_shape>>=
# SA = sampling area
#dens.sa.N = density in SA using N (estimated number of individuals)
cc$dens.sa.N <- with(cc, N/area.sa)
# density in sampling area using true number of centroids inside SA
cc$dens.sa.true <- with(cc, ncent.sa/area.sa)  # true density
# density in SA enlarged for sigma using N
cc$dens.sa.sigma.N <- with(cc, N/area.sa.sigma)
# density in SA enlarged for sigma using true number of centroids inside SA
cc$dens.sa.sigma.true <- with(cc, ncent.sa.sigma/area.sa.sigma) # true density
# density in SA enlarged for MMDM using N
cc$dens.sa.mmdm.N <- with(cc, N/area.sa.mmdm)
# density in SA enlarged for MMDM using true number of centroids inside SA
cc$dens.sa.mmdm.true <- with(cc, ncent.sa.mmdm/area.sa.mmdm) # true density

cc$dens.sa <- with(cc, dens.sa.N - dens.sa.true)
cc$dens.sa.sigma <- with(cc, dens.sa.sigma.N - dens.sa.sigma.true)
cc$dens.sa.mmdm <- with(cc, dens.sa.mmdm.N - dens.sa.mmdm.true)

dens.cc <- melt(cc, measure.vars = c("dens.sa", "dens.sa.sigma", "dens.sa.mmdm"))
@

<<facetbysigma_vary_sa, echo = FALSE, fig.width = 20, fig.height = 20>>=
dummy <- unique(dens.cc[, c("sigma", "variable", "b")])
dummy$value <- dummy$r <- 1

ggplot(dens.cc, aes(x = r, y = value)) +
    theme_bw() +
    geom_rect(data = dummy, aes(fill = variable), xmin = -Inf, xmax = Inf, ymin = -Inf,
              ymax = Inf, alpha = 0.05, show_guide = FALSE) +
    ylab("Estimated density - simulated density of centroids") +
    geom_point(size = 2, shape = 1, alpha = 0.5) +
    geom_smooth(se = FALSE, color = "red", method = "loess") +
    geom_vline(xintercept = unique(cc$arena), size = 0.25) +
    facet_grid(sigma ~ variable)
@

Above figure shows bias of density estimate in relation
to size of sampling area ($r$), enlargement of it (columns) and the size of
home range (rows).
Presence of no bias would be indicated by values lying close to zero. Bias
becomes negligible at around sampling area size of 5 (size of arena,
noted as a vertical line) even for vanilla (\texttt{dens.sa}) sampling area.
Sampling area enlarged for sigma or MMDM show far less bias. For home range size
of 1 and 3, there is some negative bias, but this dissipates for sampling areas
enlarged for sigmas, compared to those enlarged for MMDM. Values for sampling
areas enlarged for MMDM (third column) seem to exhibit a bit more variance than
those enlarged for sigma (second column).

<<facetbysigma_b_vary_sa, echo = FALSE>>=
ggplot(dens.cc, aes(x = r, y = value)) +
    theme_bw() +
    geom_rect(data = dummy, aes(fill = variable), xmin = -Inf, xmax = Inf, ymin = -Inf,
              ymax = Inf, alpha = 0.2, show_guide = FALSE) +
    ylab("Estimated density - simulated density of centroids") +
    geom_point(size = 2, shape = 1, alpha = 0.5) +
    geom_smooth(se = FALSE, color = "red", method = "loess") +
    geom_vline(xintercept = unique(cc$arena), size = 0.25) +
    facet_grid(sigma ~ variable + b)
#ggsave("full.figure.pdf", width = 20, height = 12)
@

Above figure shows (a) bias ($y$ axis) in relation to (b) sampling area ($x$
axis), sliced by (c) vanilla sampling area, sampling area enlarged for sigma and
sampling area enlarged for MMDM (red, green and blue columns, respectively) and
(d) according to home range size (rows). Each sampling area (c) is further
sliced according to the (e) deformation of the sampling area by parameter
\texttt{b}. Higher the values, the more ellipsoid the sampling area.

\newpage
\bibliographystyle{authordate1}
\bibliography{q:/clanki/bazaclankov}

\end{document}
