#library(sp)
#library(rgeos)
#library(spatstat)
#library(ars)
#library(car)
#library(RMark)
#library(secr) # ?closedN, ?read.caphist
##library(scrbook) # https://sites.google.com/site/spatialcapturerecapture/scrbook-r-package
#library(ggplot2)

setwd("./simulation")

library(devtools)
#library(rstudioapi)
load_all("q:/workspace/edgeEffect/")
#document("q:/workspace/edgeEffect/", clean = TRUE)
nsim <- 2000

set.seed(357)
runsim <- data.frame(
   a = 1, 
   b = 1,
   r = runif(n = nsim, min = 2, max = 15), 
   arena = 5, 
   world = 40, 
   N = round(runif(nsim, min = 20, max = 300)), 
   prob = runif(n = nsim, min = 0.1, max = 0.7), 
   J = round(runif(n = nsim, min = 4, max = 10)),
   space.use = "halfnormal",
   sigma = sample(c(1, 3, 6, 9, 12, 15), size = nsim, replace = TRUE),
   two.groups = FALSE,
   seed = round(runif(n = nsim, min = 1, max = 1000000)),
   filename = "result_vary_sigma_from_2_to_15_by_3")
result <- vector("list", nsim)

set.seed(762)
runsim <- data.frame(
   a = 1, 
   b = sample(c(1, 2, 5, 15), size = nsim, replace = TRUE),
   r = runif(n = nsim, min = 2, max = 15), 
   arena = 5, 
   world = 40, 
   N = round(runif(nsim, min = 20, max = 300)), 
   prob = runif(n = nsim, min = 0.1, max = 0.7), 
   J = round(runif(n = nsim, min = 4, max = 10)),
   space.use = "halfnormal",
   sigma = sample(c(1, 3, 6, 9, 12, 15), size = nsim, replace = TRUE),
   two.groups = FALSE,
   seed = round(runif(n = nsim, min = 1, max = 1000000)),
   filename = "result_vary_sa_shape_from_circle_to_ellipse")
result <- vector("list", nsim)

set.seed(3006)
runsim <- data.frame(
   a = 1, 
   b = 1,
   r = runif(n = nsim, min = 2, max = 15), 
   arena = 5, 
   world = 40, 
   N = round(runif(nsim, min = 20, max = 300)), 
   prob = runif(n = nsim, min = 0.1, max = 0.7), 
   J = round(runif(n = nsim, min = 4, max = 10)),
   space.use = "halfnormal",
   sigma = sample(c(1, 3, 6, 9, 12, 15), size = nsim, replace = TRUE),
   two.groups = FALSE,
   seed = round(runif(n = nsim, min = 1, max = 1000000)),
   filename = "result_vary_sigma_from_2_to_15_by_3")

result <- vector("list", nsim)
system.time(
   for (i in 1:nsim) {
      if (i %% 100 == 0) {
         message(paste("Iteration number", i, "at", Sys.time()))
         takeout <- list.files(pattern = "mark")
         file.remove(takeout[grepl("mark", takeout)]) # clean once in a while
      }
      result[[i]] <- edgeEffect(
         a = runsim[i, "a"],
         b = runsim[i, "b"],
         r = runsim[i, "r"],
         arena = runsim[i, "arena"],
         world = runsim[i, "world"],
         N = runsim[i, "N"],
         prob = runsim[i, "prob"],
         J = runsim[i, "J"], 
         space.use = runsim[i, "space.use"],
         sigma = runsim[i, "sigma"],
         two.groups = runsim[i, "two.groups"],
         seed = runsim[i, "seed"],
         verbose = FALSE,
         filename = runsim[i, "filename"]
      )
   })



# uniformno
set.seed(38)
runsim <- data.frame(
   a = 1, 
   b = 1,
   r = runif(n = nsim, min = 2, max = 15), 
   arena = 5, 
   world = 40, 
   N = round(runif(nsim, min = 20, max = 300)), 
   prob = runif(n = nsim, min = 0.1, max = 0.7), 
   J = round(runif(n = nsim, min = 4, max = 10)),
   space.use = "uniform",
   sigma = sample(c(1, 3, 6, 9, 12, 15), size = nsim, replace = TRUE),
   two.groups = FALSE,
   seed = round(runif(n = nsim, min = 1, max = 1000000)),
   filename = "result_vary_uniform_from_2_to_15_by_3")

result <- vector("list", nsim)
system.time(
   for (i in 1:nsim) {
      if (i %% 100 == 0) {
         message(paste("Iteration number", i, "at", Sys.time()))
         takeout <- list.files(pattern = "mark")
         file.remove(takeout[grepl("mark", takeout)]) # clean once in a while
      }
      result[[i]] <- edgeEffect(
         a = runsim[i, "a"],
         b = runsim[i, "b"],
         r = runsim[i, "r"],
         arena = runsim[i, "arena"],
         world = runsim[i, "world"],
         N = runsim[i, "N"],
         prob = runsim[i, "prob"],
         J = runsim[i, "J"], 
         space.use = runsim[i, "space.use"],
         sigma = runsim[i, "sigma"],
         two.groups = runsim[i, "two.groups"],
         seed = runsim[i, "seed"],
         verbose = FALSE,
         filename = runsim[i, "filename"]
      )
   })


# uniformno z elipsami
set.seed(22)
runsim <- data.frame(
   a = 1, 
   b = sample(c(1, 2, 5, 15), size = nsim, replace = TRUE),
   r = runif(n = nsim, min = 2, max = 15), 
   arena = 5, 
   world = 40, 
   N = round(runif(nsim, min = 20, max = 300)), 
   prob = runif(n = nsim, min = 0.1, max = 0.7), 
   J = round(runif(n = nsim, min = 4, max = 10)),
   space.use = "uniform",
   sigma = sample(c(1, 3, 6, 9, 12, 15), size = nsim, replace = TRUE),
   two.groups = FALSE,
   seed = round(runif(n = nsim, min = 1, max = 1000000)),
   filename = "result_uniform_hr_vary_sa_shape_from_circle_to_ellipse")

result <- vector("list", nsim)
system.time(
   for (i in 1:nsim) {
      if (i %% 100 == 0) {
         message(paste("Iteration number", i, "at", Sys.time()))
         takeout <- list.files(pattern = "mark")
         file.remove(takeout[grepl("mark", takeout)]) # clean once in a while
      }
      result[[i]] <- edgeEffect(
         a = runsim[i, "a"],
         b = runsim[i, "b"],
         r = runsim[i, "r"],
         arena = runsim[i, "arena"],
         world = runsim[i, "world"],
         N = runsim[i, "N"],
         prob = runsim[i, "prob"],
         J = runsim[i, "J"], 
         space.use = runsim[i, "space.use"],
         sigma = runsim[i, "sigma"],
         two.groups = runsim[i, "two.groups"],
         seed = runsim[i, "seed"],
         verbose = FALSE,
         filename = runsim[i, "filename"]
      )
   })
 