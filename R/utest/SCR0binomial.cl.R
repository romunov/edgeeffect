SCR0binomial.cl <- function (y, X, M, xl, xu, yl, yu, K, delta, niter)  {
   S <- cbind(runif(M, xl, xu), runif(M, yl, yu))
   sigma <- runif(1, 5, 10)
   lam0 <- runif(1, 0.1, 1)
   psi <- runif(1, 0.2, 0.8)
   z <- rbinom(M, 1, psi)
   seen <- apply(y > 0, 1, any)
   z[seen] <- 1
   K = K
   delta = delta
   d <- e2dist(S, X)
   lam <- lam0 * exp(-(d * d)/(2 * sigma * sigma))
   pmat <- 1 - exp(-lam)
   pmat[pmat < 1e-30] <- 1e-30
   out <- matrix(nrow = niter, ncol = 4)
   colnames(out) <- c("sigma", "lam0", "psi", "N")
   cat("\nstarting values =", c(sigma, lam0, psi, sum(z), "\n\n"))
   for (iter in 1:niter) {
      if (iter%%100 == 0) {
         cat("iter", iter, format(Sys.time(), "%H:%M:%S"), 
            "\n")
         cat("   current =", out[iter - 1, ], "\n")
      }
      sig.cand <- rnorm(1, sigma, delta[1])
      if (sig.cand > 0) {
         lam.cand <- lam0 * exp(-(d * d)/(2 * sig.cand * sig.cand))
         p.cand <- 1 - exp(-lam.cand)
         ll <- sum(dbinom(y, K, pmat * z, log = TRUE))
         llcand <- sum(dbinom(y, K, p.cand * z, log = TRUE))
         if (runif(1) < exp(llcand - ll)) {
            ll <- llcand
            pmat <- p.cand
            sigma <- sig.cand
         }
      }
      lam0.cand <- rnorm(1, lam0, delta[2])
      if (lam0.cand > 0) {
         lam.cand <- lam0.cand * exp(-(d * d)/(2 * sigma *  sigma))
         p.cand <- 1 - exp(-lam.cand)
         llcand <- sum(dbinom(y, K, p.cand * z, log = TRUE))
         if (runif(1) < exp(llcand - ll)) {
            pmat <- p.cand
            lam0 <- lam0.cand
         }
      }
      zUps <- 0
      for (i in 1:M) {
         if (seen[i]) 
            next
         zcand <- ifelse(z[i] == 0, 1, 0)
         llz <- sum(dbinom(y[i, ], K, pmat[i, ] * z[i], log = TRUE))
         llcand <- sum(dbinom(y[i, ], K, pmat[i, ] * zcand, log = TRUE))
         prior <- dbinom(z[i], 1, psi, log = TRUE)
         prior.cand <- dbinom(zcand, 1, psi, log = TRUE)
         if (runif(1) < exp((llcand + prior.cand) - (llz +  prior))) {
            z[i] <- zcand
            zUps <- zUps + 1
         }
      }
      psi <- rbeta(1, 1 + sum(z), 1 + M - sum(z))
      Sups <- 0
      for (i in 1:M) {
         Scand <- c(rnorm(1, S[i, 1], delta[3]), rnorm(1,  S[i, 2], delta[3]))
         inbox <- Scand[1] < xu & Scand[1] > xl & Scand[2] <  yu & Scand[2] > yl
         if (inbox) {
            dtmp <- sqrt((Scand[1] - X[, 1])^2 + (Scand[2] -  X[, 2])^2)
            lam.cand <- lam0 * exp(-(dtmp * dtmp)/(2 * sigma *  sigma))
            p.cand <- 1 - exp(-lam.cand)
            llS <- sum(dbinom(y[i, ], K, pmat[i, ] * z[i],  log = TRUE))
            llcand <- sum(dbinom(y[i, ], K, p.cand * z[i],  log = TRUE))
            if (runif(1) < exp(llcand - llS)) {
               S[i, ] <- Scand
               pmat[i, ] <- p.cand
               d[i, ] <- dtmp
               Sups <- Sups + 1
            }
         }
      }
      if (iter%%100 == 0) {
         cat("   Acceptance rates\n")
         cat("     z =", zUps/M, "\n")
         cat("     S =", Sups/M, "\n")
      }
      out[iter, ] <- c(sigma, lam0, psi, sum(z))
   }
   return(out)
}

data(beardata)

#set data augmentation 
M <- 700

trapmat <- beardata$trapmat

#summarize capture across occasions
bearmat <- apply(beardata$bearArray, 1:2, sum)
#augment
Xaug <- matrix(0, nrow=M, ncol=dim(trapmat)[1])
Xaug[1:dim(bearmat)[1],]<-bearmat

#set state space coordinates

xl<-min(trapmat[,1])-20
xu<-max(trapmat[,1])+20
yl<-min(trapmat[,2])-20
yu<-max(trapmat[,2])+20

set.seed(2013)

#run MCMC algorithm
system.time(mod0<-SCR0binom.cl(y=Xaug,X=trapmat,M=M, xl=xl,xu=xu,yl=yl,yu=yu,K=dim(beardata$bearArray)[3],
   delta=c(0.1, 0.05, 2), niter=5000))

#use coda to look at output
library(coda)
summary(window(mcmc(mod0), start=1001))
